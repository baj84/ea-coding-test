# EA Coding Test

## Architecture
 - Webpack
 - React
 - Redux
 - Jest
 - NPM
 - ESLint
 - Babel

I realise that the spec was to write a service, but decided on implementing a solution that uses react & redux to demonstrate my competency. 

## Install

Install required node modules: `npm install` then simply `npm start`

## CORS Issue
Because this project is run from localhost there is a CORS issue and the server doens't allow the remote origin. So i've had to mock the endpoint, but i've included the code (commented out in carModelActions.js) to demonstrate how I would call the API.

## Tests
I've included just a few simple tests to demonstrate how I would go about unit testing this project.

To run them use `npm test`
