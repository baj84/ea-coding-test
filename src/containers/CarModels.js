import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import * as actions from '../actions/carModelActions';
import CarModelList from '../components/CarModelList';

class CarModels extends React.Component {
  componentDidMount() {
    this.props.actions.loadCarModels();
  }

  render() {
    const { isLoading, cars } = this.props;

    return (
      <div>
        { isLoading
            ? <div>Loading...</div>
            : <CarModelList cars={this.props.cars}></CarModelList>
        }
      </div>
    );
  }
}

CarModels.propTypes = {
  actions: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  return {
    isLoading: state.carModels.isLoading,
    cars: state.carModels.cars
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CarModels);
