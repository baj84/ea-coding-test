import React from 'react';

import CarModelsContainer from '../Containers/CarModels';

class App extends React.Component {
  render() {
    return (
      <div>
        <CarModelsContainer />
      </div>
    );
  }
}

export default App;