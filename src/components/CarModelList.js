import React from 'react';
import PropTypes from 'prop-types';

class CarModelList extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        {this.props.cars.map((car, index) => {
            return (
              <ul key={`makes-${index}`}>
                <li><strong>{car.make}</strong></li>
                <ul>
                  {car.models.map((model, index) => {
                      return (
                        <div key={`model-${index}`}>
                          <li>{model.model}</li>
                          <ul>
                            {model.shows.map((show, index) => {
                              return <li key={`show-${index}`}>{show}</li>
                            })}
                          </ul>
                        </div>
                      )
                    })
                  }
                </ul>
              </ul>
            )
          })
        }
      </div>
    );
  }
}

CarModelList.propTypes = {
  cars: PropTypes.array.isRequired
};

export default CarModelList;
