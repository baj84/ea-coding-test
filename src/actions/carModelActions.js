import axios from 'axios';

import * as actionTypes from '../constants/actionTypes';
import { createAction} from '../helpers/redux';

import { API_URL } from '../constants/settings';


/**
 * Sorts an array of objects
 *
 * @param {Array} data
 * @param {String} key
 * @returns {Array}
 */
export function sortAlphabetically(data, key) {
  return data.sort((a, b) => {
    if(a[key] < b[key]) { return -1; }
    if(a[key] > b[key]) { return 1; }

    return 0;
  });
}

/**
 * Finds an item in an array
 *
 * @param {Array} haystack
 * @param {String} needle
 * @param {String} key
 * @returns {Object}
 */
export function findItem(haystack, needle, key) {
  return haystack.find(item => item[key] === needle);
}

/**
 * Groups the cars by makes
 *
 * @param {Array} cars
 * @returns {Array}
 */
export function groupByMake(cars) {
  let grouped = [];

  cars.forEach(show => {
    show.cars.forEach(car => {
      let make = findItem(grouped, car.make, 'make');

      // Add the new make to our array
      if (!make) {
        make = { make: car.make, models: [] };
        grouped.push(make);
      }

      const model = findItem(make.models, car.model, 'model');

        // Add the new model to our array
      if (!model) {
        make.models.push({ model: car.model, shows: [show.name] });
      }

      // Only add the show if it doesn't already exist for this model
      if (model && model.shows.indexOf(show.name) === -1) {
        model.shows.push(show.name);
      }
    });
  });

  return sortAlphabetically(grouped, 'make');
}

/**
 * Loads the car models from the API
 *
 * @returns {Function}
 */
export function loadCarModels() {
  return async function (dispatch) {
    dispatch(createAction(actionTypes.LOAD_CAR_MODELS_PENDING));

    // TODO This is commented out due to CORS issue. As a workaround
    // i'm simply mocking the data.

    // try {
    //   const response = await axios.get(`${API_URL}/cars`, {
    //     'Content-Type': 'application/json',
    //     'Access-Control-Allow-Origin': '*'
    //   });
    // } catch (error) {
    //   throw error;
    // }

    const cars = require('../constants/mockData.json');

    const groupedCars = groupByMake(cars);
    
    dispatch({ type: actionTypes.LOAD_CAR_MODELS_SUCCESS, payload: groupedCars });
  };
}

