import * as axios from 'axios';
import * as actionTypes from '../constants/actionTypes';
import * as actionCreators from './carModelActions';

describe('CarModel Actions', () => {
  const appState = {
    isLoading: false,
    cars: ['car1']
  };

  it('loadCarModels() should dispatch a pending action', () => {
    const dispatch = jest.fn();
    const expected = {
      type: actionTypes.LOAD_CAR_MODELS_PENDING
    };

    const getState = () => { return { carModels: appState } };

    expect(typeof(actionCreators.loadCarModels(appState))).toEqual('function');

    actionCreators.loadCarModels(appState)(dispatch, getState);

    expect(dispatch).toBeCalledWith(expected);
  });

  // TODO Tests is commented out due to CORS issue.

  // it('loadCarModels() expects axios to have been called once', () => {
  //   const dispatch = jest.fn();
  //   const getState = () => { return { carModels: appState } };
    
  //   jest.mock('axios');
  //   axios.get.mockImplementation(() =>
  //     Promise.resolve({
  //       data: { results: ['car1'] }
  //     })
  //   );

  //   actionCreators.loadCarModels(appState)(dispatch, getState);

  //   expect(axios.get).toHaveBeenCalledTimes(1);
  // });
});
