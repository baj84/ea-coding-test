import { createReducer } from '../helpers/redux';

import * as actionTypes from '../constants/actionTypes'; 

const initialState = {
  isLoading: true,
  cars: []
};

export default createReducer(initialState, {
  [actionTypes.LOAD_CAR_MODELS_PENDING]: (state) => {
    return {
      ...state,
      isLoading: true
    };
  },
  [actionTypes.LOAD_CAR_MODELS_SUCCESS]: (state, cars) => {
    return {
      ...state,
      isLoading: false,
      cars
    };
  }
});
