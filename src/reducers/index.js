import { combineReducers } from 'redux';

import carModels from './carModelsReducer';

const rootReducer = combineReducers({
  carModels
});

export default rootReducer;
