import * as actionTypes from '../constants/actionTypes';
import reducer from './carModelsReducer';

describe('Game Reducer', () => {
  const getInitialState = () => {
    return {
      isLoading: false,
      cars: []
    };
  };

  const getAppState = () => {
    return {
      isLoading: false,
      cars: [],
    };
  };

  it('LOAD_CAR_MODELS_SUCCESS: Should assign cars to state', () => {
    const cars = [ 'car1', 'car2' ];

    const action = { type: actionTypes.LOAD_CAR_MODELS_SUCCESS, payload: cars };
    const expected = Object.assign(getAppState(), { cars });

    expect(reducer(getAppState(), action)).toEqual(expected);
  });
});
